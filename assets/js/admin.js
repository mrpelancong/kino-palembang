function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split       = number_string.split(','),
  sisa        = split[0].length % 3,
  rupiah        = split[0].substr(0, sisa),
  ribuan        = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}


function message(title, msg, type, reload) {
    var dismiss = '';
    if (type === 'success') {
        dismiss = 'true';
    } else {
        dismiss = 'false';
    }

    $.notify({
      icon: 'fa fa-info-circle',
      title: title,
      message: msg
    },{
      type: type,
      allow_dismiss: dismiss,
      placement: {
        from: 'top',
        align: 'right'
      },
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutUp'
      }
    });

  }

// icon change
$("#editIcon").on('show.bs.modal', function(event){
  var modal =$(this);

  $(".cek_logo").keyup( function(){
    var icon = $(this).val();
    modal.find("#icon").attr("class", icon);
  })
  // 
  var button = $(event.relatedTarget) 
  var value = button.data('value')
  var table = button.data('tb')
  if (table === 'group') {
    var table_cek = table+"_menu";
  } else {
    table_cek = table;
  }
  var params = button.data('params') 
  var field_icon = button.data('icon')
  $("#table").val("tb_"+table_cek)

  $.ajax({
      url: "read.php",
      type: "GET",
      data : { "type": "server", 
        "tb": table_cek, 
        "params": params, 
        "value": value },
      success: function (ajaxData){
        var obj = jQuery.parseJSON(ajaxData);
        console.log(obj);
        if (obj.status != 0) {
          $('#id_icon').val(obj[0]['id_'+table])
          $('#icon').attr("class", obj[0][field_icon])
          $('.cek_logo').val(obj[0][field_icon])
        } else {
          alert(obj.msg);
        }
      }
  })
})

$("#editIcon").on('hide.bs.modal', function(){
  $(".cek_logo").val('');
  var modal =$(this);
  modal.find("#icon").attr("class", "fa fa-firefox");
})


// 
$(document).on('click', '#edit-menu', function(){
  var m = $(this).attr('user-data');
  var table = $(this).attr('user-table');
  var params = $(this).attr('user-params');
  var type = $(this).attr('user-type');

  $.ajax({
      url: "read.php",
      type: "GET",
      data : {"id": m, "table": table, "params": params, "type": type},
      success: function (ajaxData){
        $("#modalEditStaf").html(ajaxData);
        $("#modalEditStaf").modal('show',{backdrop: 'true'});
        var obj = jQuery.parseJSON(ajaxData);
        $('#id_menu').val(obj[0]['id_menu'])
        $('#menu').val(obj[0]['menu'])
        $('#param_get').val(obj[0]['param_get'])
        $('#urutan').val(obj[0]['urutan'])
        $('#icon').val(obj[0]['icon'])
      }
  })
})

/*Ajax Website Client*/
var data = '';
var no = 1; //Numbering

// $(".BEanggota").click( function(){

$('#editakses').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var value = button.data('value')
  var table = button.data('tb')
  var params = button.data('params') 
  var modal = $(this)

  // alert(value+table+params)
  $.ajax({
      url: "read.php",
      type: "GET",
      data : { "type": "server", 
        "tb": table, 
        "params": params, 
        "value": value },
      success: function (ajaxData){
        var obj = jQuery.parseJSON(ajaxData);
        console.log(obj);
        if (obj.status != 0) {
          $('#id_data').val(obj[0]['id_akses'])
          $('.akses').val(obj[0]['akses'])
          $('.keterangan_akses').val(obj[0]['keterangan_akses'])
        } else {
          alert(obj.msg);
        }
      }
  })
})

$('#editadmin').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var value = button.data('value')
  var table = button.data('tb')
  var params = button.data('params') 
  var modal = $(this)

  // alert(value+table+params)
      $.ajax({
          url: "read.php",
          type: "GET",
          data : { "type": "client", 
            "tb": table, 
            "params": params, 
            "value": value },
          success: function (ajaxData){
            var obj = jQuery.parseJSON(ajaxData);
            console.log(obj);
            if (obj.status != 0) {
              $('#id_data').val(obj[0]['id_admin'])
              $('.akses_id').val(obj[0]['akses_id'])
              $('.nama_lengkap').val(obj[0]['nama_lengkap'])
              $('.email_reset').val(obj[0]['email_reset'])
              $('.nama_pengguna').val(obj[0]['nama_pengguna'])
              $('.password').val(obj[0]['password'])
            } else {
              console.log(obj);
            }
          }
      })
  })


$("#gantiPassword").on('show.bs.modal', function(){
  $(".password").change( function(){
      var password =  $(this).val();
      jumlah = password.length;
      if (jumlah < 8) {
        $(".hasil").html('<div class="alert alert-danger"><strong><i class="fa fa-info-circle"></i></strong> Password minimal delapan(8) karakter</div>');
        $(".password").val('');
      } else {
        $(".hasil").html('<div class="alert alert-success"><strong><i class="fa fa-info-circle"></i></strong> Password bagus, silahkan ulangi password.</div>');
      }
  })

  $(".repassword").change( function(){
    var password = $(".password").val();
    var repassword = $(".repassword").val();
    if (password === repassword) {
      $(".hasil").html('<div class="alert alert-success"><strong><i class="fa fa-info-circle"></i></strong> Password bagus, silahkan klik reset password.</div>');
    } else {
      $(".hasil").html('<div class="alert alert-danger"><strong><i class="fa fa-info-circle"></i></strong> Password tidak sama! Ulangi lagi.</div>');
       $(".password").val('');
       $(".repassword").val('');
    }
  })
})


$('#tambahAkses').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) 
    var recipient = button.data('akses') 
    
    var modal = $(this)
    modal.find('#akses_id').val(recipient)
    $("#group_id").change( function(){
      var group_id = $(this).val();
      $('.custom-control-input').prop('checked', false);
      $.ajax({
        url: "read.php?akses=menu",
        type: "GET",
        data : {
          "value": recipient,
          "value2" : group_id },
        success: function (ajaxData){
          var obj = jQuery.parseJSON(ajaxData);
          console.log(obj);
          if (obj.status === 0) {
            $('.custom-control-input').prop('checked', false);
          } else {
            for (var i = obj.length - 1; i >= 0; i--) {
               var item = obj[i];
               var data =  $("#customCheck"+item.menu_id).val();
               if (data == item.menu_id) {
                  $('#customCheck'+item.menu_id).prop('checked', true);
               }
            }
          }
          // console.log(obj.length);
        }
      })
    })
        // alert(recipient);
})

$("#tambahAkses").on('hidden.bs.modal', function(){
  $('.custom-control-input').prop('checked', false);
})

