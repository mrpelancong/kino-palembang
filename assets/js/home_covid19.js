var kasus_table = $('#kasus_covid19_t').DataTable( {
	"paging":   false,
	"searching": false,
	"info": false
});

var status_table = $('#status_korban_t').DataTable( {
	"paging":   false,
	"searching": false,
	"info": false
});

var odp_table = $('#odp_korban_t').DataTable( {
	"paging":   false,
	"searching": false,
	"info": false
});

var pdp_table = $('#pdp_korban_t').DataTable( {
	"paging":   false,
	"searching": false,
	"info": false
});

var positif_table = $('#positif_korban_t').DataTable( {
	"paging":   false,
	"searching": false,
	"info": false
});

    // simpan record kasus baru
    $(".record_kasusbtn").click(function(){
    	var data_kasus = $(".from_kasus").find("form").serialize();
	    // console.log(data_kasus);
	    $.ajax({
	        type: 'POST', //post method
	        url: base_url+'share/api/record_kasus/',
	        data: data_kasus,
	        dataType: "json",
	        beforeSend: function(result) {
	        	message('Record Data', 'info', true);
	        	$(".from_kasus").find(".form-control").attr('readonly', true);
	        },
	        success: function(result, textStatus, jqXHR) {
                console.log(result);
                if (result.data_status === false) {
                    message(result.msg, 'danger', false);
                } else {
                    message(result.msg, 'success', true);
                }
                $(".from_kasus").find(".form-control").val(null);
                $(".from_kasus").find(".form-control").removeAttr('readonly');
                    // refresh table kasus
                    list_kasus(master_wilayah_id);
                }
            });
	})

    $(".kode_kasus").change(function(){
    	var kode_kasus = $(this).val();
    	cek_kode(kode_kasus);
    })

    function status_korban(id_korban) {
    	detail_kasus(id_korban);

    	fnodp_table(id_korban);
    	fnpdp_table(id_korban);
    	fnpositif_table(id_korban);

    // open form
    $(".keterangan_korban").find(".form-control").removeAttr("disabled");
};

function cek_kode(kode_kasus){
	$.ajax({
        type: 'GET', //post method
        url: base_url+'share/api/record_kasus/kd/'+kode_kasus, //ajaxformexample url
        dataType: "json",
        beforeSend: function(result) {
        	console.log("Loading Data");
        },
        success: function(result, textStatus, jqXHR) {
            // console.log(result);
            // alert(result.data_status);
            if (result.data_status === true) {
            	$(".kode_kasus").val('').focus();
            	message('kode Kasus Sudah Ada', 'danger', false)
            }
        }
    });
}

function list_kasus(master_wilayah_id){
	kasus_table.destroy();
	kasus_table = $('#kasus_covid19_t').DataTable({
		"processing": true,
		"paging":   true,
		"searching": true,
		"info": true,
		"ajax": {
			"url": base_url+"share/api/list_kasus/id/"+master_wilayah_id,
			"dataSrc": "data"
		},
		"order": [
		[1, 'desc']
		],
		"columns": [
		{
			"data": null
		},
		{
			"data": "kode_korban"
        },
        {
            "data": null,
            "render" : function(data, type, row){
               var jenis_kelamin = "";
               if (row.jenis_kelamin == 'Pria') {
                   jenis_kelamin = "<strong class='text-danger'>"+row.jenis_kelamin+"</strong>";
               } else {
                   jenis_kelamin = "<strong class='text-info'>"+row.jenis_kelamin+"</strong>";
               }
               return jenis_kelamin+" [<strong>"+row.usia_korban+"</strong>]<br><em>"+row.kecamatan.replace("Kecamatan","")+"</em>";
           }
       },
       {
           "data": "tanggal_masuk"
       },
       {
           "data": null,
           "render": function (data, type, row) {
              var button = "<button data-id='"+row.id_korban+"' class='btn btn-primary btn-block btn-sm' onclick='status_korban("+row.id_korban+")'> <i class='fa fa-shield'></i> Update</button>";

              return button;
          }
      },
      {
       "data": null,
       "render": function (data, type, row) {
          var button = "<center><a href='#' data-id='"+row.id_korban+"' class='edit text-success' data-toggle='modal' data-target='#editData'> <i class='fa fa-edit '></i></a></center>";

          return button;
      }
  }
  ]
});
	kasus_table.on('order.dt search.dt', function() {
		kasus_table.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function(cell, i) {
			cell.innerHTML = i + 1;
		});
	}).draw();
}

function detail_kasus(id_korban){
	$(".korban_id").val(id_korban);
	$.ajax({
        type: 'GET', //post method
        url: base_url+'share/api/record_kasus/id/'+id_korban, //ajaxformexample url
        dataType: "json",
        beforeSend: function(result) {
        	console.log("Loading Data");
        },
        success: function(result, textStatus, jqXHR) {
        	// console.log(result)
        	$("#detail").text(result.data[0].kode_korban+'/'+result.data[0].jenis_kelamin+'/'+result.data[0].usia_korban);
        	$("#tanggal_masuk").text(result.data[0].tanggal_masuk);
        	$("#nama_wilayah").text(result.data[0].asal);
        	$("#status").text(result.data[0].status.status_k);
        	if (result.data[0].record.status.length != 0) 
        	{
        		var status_k = result.data[0].record.status[0].status;
        	} else {
        		var status_k = null;
        	}
        	$(".status_korban").val(status_k);
            // style
            $("#status").removeClass('bg-info bg-danger bg-warning bg-success bg-grey').addClass('bg-'+result.data[0].status.style);
        }
    });
}

function fnstatus_table(id_korban) {
	status_table.destroy();
	status_table = $('#status_korban_t').DataTable({
		"processing": true,
        // "serverSide": true,
        "paging":   false,
        "searching": false,
        "info": false,
        "ajax": {
        	"url": base_url+"share/api/record_kasus/id/"+id_korban,
        	"dataSrc": "data.0.record.odp"
        },
        "order": [
        [0, 'desc']
        ],
        "columns": [
        {
        	"data": "tanggal_update"
        },
        {
        	"data": null,
        	"render": function (data, type, row) {
        		if ( row.status === '0') {
        			return 'ODP';
        		} else if (row.status === '1') {
        			return 'PDP';
        		} else if (row.status == '2') {
        			return 'Positif';
        		} else {
        			return 'Sembuh';
        		}
        	}
        },
        ]
    });

    // console.log(ajax);
}

function fnodp_table(id_korban) {
	odp_table.destroy();
	odp_table = $('#odp_korban_t').DataTable({
		"processing": true,
        // "serverSide": true,
        "paging":   false,
        "searching": false,
        "info": false,
        "ajax": {
        	"url": base_url+"share/api/record_kasus/id/"+id_korban,
        	"dataSrc": "data.0.record.odp"
        },
        "order": [
        [0, 'desc']
        ],
        "columns": [
        {
        	"data": "tanggal_update"
        },
        {
        	"data": null,
        	"render": function (data, type, row) {
        		if ( row.status === '0') {
        			return 'Proses Pemantauan';
        		} else {
        			return 'Pemantauan Selesai';
        		}
        	}
        },
        ]
    });
    // console.log(ajax);

}

function fnpdp_table(id_korban) {
	pdp_table.destroy();
	pdp_table = $('#pdp_korban_t').DataTable({
		"processing": true,
        // "serverSide": true,
        "paging":   false,
        "searching": false,
        "info": false,
        "ajax": {
        	"url": base_url+"share/api/record_kasus/id/"+id_korban,
        	"dataSrc": "data.0.record.pdp"
        },
        "order": [
        [0, 'desc']
        ],
        "columns": [
        {
        	"data": "tanggal_update"
        },
        {
        	"data": null,
        	"render": function (data, type, row) {
        		if ( row.status === '0') {
        			return 'Proses Perawatan';
        		} else if(row.status === '1') {
        			return 'Negatif';
        		} else {
        			return 'Positif';
        		}
        	}
        },
        ]
    });

}

function fnpositif_table(id_korban) {
	positif_table.destroy();

	positif_table = $('#positif_korban_t').DataTable({
		"processing": true,
        // "serverSide": true,
        "paging":   false,
        "searching": false,
        "info": false,
        "ajax": {
        	"url": base_url+"share/api/record_kasus/id/"+id_korban,
        	"dataSrc": "data.0.record.positif"
        },
        "order": [
        [0, 'desc']
        ],
        "columns": [
        {
        	"data": "tanggal_update"
        },
        {
        	"data": null,
        	"render": function (data, type, row) {
        		if ( row.status === '0') {
        			return 'Dirawat';
        		} else if(row.status === '1') {
        			return 'Sembuh';
        		} else {
        			return 'Meninggal';
        		}
        	}
        },
        ]
    });

}