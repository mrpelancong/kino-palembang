<?php

/*
 * English language
 */

$lang['text_rest_invalid_api_key'] = 'Kunci API tidak valid'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Hak kredensial tidak valid';
$lang['text_rest_ip_denied'] = 'IP ditolak';
$lang['text_rest_ip_unauthorized'] = 'IP tidak diberi kuasa';
$lang['text_rest_unauthorized'] = 'Tidak diberi kuasa';
$lang['text_rest_ajax_only'] = 'Hanya AJAX requests yang diperbolehkan';
$lang['text_rest_api_key_unauthorized'] = 'Kunci API ini tidak memiliki akses ke Controller yang diminta';
$lang['text_rest_api_key_permissions'] = 'Kunci API ini tidak memiliki izin akses yang cukup';
$lang['text_rest_api_key_time_limit'] = 'Kunci API ini telah mencapai batas waktu untuk mengakses metode ini';
$lang['text_rest_ip_address_time_limit'] = 'Alamat API ini telah mencapai batas waktu untuk mengakses metode ini';
$lang['text_rest_unknown_method'] = 'Metode yang tidak ketahui';
$lang['text_rest_unsupported'] = 'Protokol tidak mendukung';
$lang['text_rest_token_not_valid'] = 'Token tidak valid';
$lang['text_rest_token_old'] = 'Token kadalwarsa';
$lang['text_rest_request_not_valid'] = 'Requests tidak valid';
$lang['text_rest_token_404'] = 'Token tidak ditemukan';
