<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_login extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function getLoginUser($username){
		$this->db->where('nama_pengguna',$username);
		$this->db->join('tb_akses','akses_id=id_akses', 'left');
		return $this->db->get('tb_user');    	
	}

}
