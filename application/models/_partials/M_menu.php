<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_menu extends CI_Model {
    
    function __construct()
    {
        parent::__construct();

    }

    function get_group($akses_id) {
    	$this->db->select('id_group_menu, nama_group, icon_group');
        $this->db->from('tb_menu_user');
        $this->db->join('tb_group_menu', 'group_menu_id=id_group_menu', 'left');
        $this->db->where('tb_menu_user.akses_id', $akses_id);
        $this->db->group_by("id_group_menu");
        return $this->db->get()->result();
    }

    function get_menu($akses_id, $params, $value){
    	$this->db->select('*, tb_menu_user.*');
        $this->db->from('tb_menu');
        $this->db->join('tb_menu_user', 'menu_id=id_menu', 'left');
        $this->db->where('tb_menu_user.akses_id', $akses_id);
        $this->db->where('status', 'active');
        $this->db->where($params, $value);
        $this->db->order_by('urutan', 'ASC');
        return $this->db->get()->result();
    }
    
    function cek_menu($akses_id, $param_get){
        $this->db->select('*');
        $this->db->from('tb_menu');
        $this->db->join('tb_menu_user', 'id_menu=menu_id', 'left');
        $this->db->where('status', 'active');
        $this->db->where('akses_id', $akses_id);
        $this->db->where('param_get', $param_get);
        return $this->db->get()->result();
    }
}

