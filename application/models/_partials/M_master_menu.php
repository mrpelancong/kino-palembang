<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// DIPAKAI UNTUK CONTROLLER MASTER_ADMIN
// CODE BY HAKIM
class M_master_menu extends CI_Model {
    function __construct()
    {
        parent::__construct();

    }
    
    public function getGroupMenu(){
        return $this->db->get('tb_group_menu')->result();       
    }
    public function getMenu(){
        return $this->db->get('tb_menu')->result();       

    }
    
    public function getMenuUser()
    {
        $this->db->join('tb_akses','tb_akses.id_akses = tb_menu_user.akses_id');
        $this->db->join('tb_menu','tb_menu.id_menu = tb_menu_user.menu_id');
        $this->db->join('tb_group_menu','tb_group_menu.id_group_menu = tb_menu_user.group_menu_id');
        $this->db->order_by('tb_akses.id_akses');
        return $this->db->get('tb_menu_user')->result();
    }
    public function insertData($table, $data)
    {
       return  $this->db->insert($table, $data);
    }
    public function updateData($table, $data, $where)
    {
        $this->db->where($where['key'], $where['value']);
         return  $this->db->update($table, $data);
        
    }
    public function deleteData($table, $where)
    {
        $this->db->where($where['key'], $where['value']);
       return  $this->db->delete($table);
        
    }
    public function getData($table)
    {
        return $this->db->get($table)->result();
    }
    public function getMenuUserWhere($id_akses)
    {
        $this->db->join('tb_akses','tb_akses.id_akses = tb_menu_user.akses_id');
        $this->db->join('tb_menu','tb_menu.id_menu = tb_menu_user.menu_id');
        $this->db->join('tb_group_menu','tb_group_menu.id_group_menu = tb_menu_user.group_menu_id');
        $this->db->order_by('tb_menu_user.group_menu_id');
        $this->db->where('tb_menu_user.akses_id',$id_akses);
        return $this->db->get('tb_menu_user')->result();
    }
}

