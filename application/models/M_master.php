<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class M_master extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getData($table, $select="*", $where=[], $join = [], $join_style = 'inner', $group_by=null, $order=[]){
		$this->db->select($select);
		$this->db->from($table);
		foreach ($where as $key => $value) {
			$this->db->where($key, $value); 
		}
		foreach ($join as $key => $value) {
			$this->db->join($key, $value, $join_style);
		}
		if (!empty($group_by)) {
			$this->db->group_by($group_by);
		}
		foreach ($order as $key => $value) {
			$this->db->order_by($key, $value);
		}
		return $this->db->get();
	}

	
	function update_data($table, $data=[], $where=[])
	{
		return $this->db->update($table, $data, $where);
	}

	function insert_data($table, $data = [])
	{
		return $this->db->insert($table, $data);
	}

	public function push_batch($tabel,$data){
        return $this->db->insert_batch($tabel, $data);
   }

	function delete_data($table, $where, $value)
	{
		return $this->db->delete($table, array($where => $value));
	}

	function deletebatch_data($table, $where)
	{
		return $this->db->delete($table, $where);
	}
}
