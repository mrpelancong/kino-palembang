<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_home extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_barChartYearly()
    {
        $this->db->select('YEAR (tanggal_lulus) as tahun, 
    CASE 
        WHEN MONTH(tanggal_lulus) = 1 THEN "Januari"
        WHEN MONTH(tanggal_lulus) = 2 THEN "Februari"
        WHEN MONTH(tanggal_lulus) = 3 THEN "Maret"
        WHEN MONTH(tanggal_lulus) = 4 THEN "April"
        WHEN MONTH(tanggal_lulus) = 5 THEN "Mei"
        WHEN MONTH(tanggal_lulus) = 6 THEN "Juni"
        WHEN MONTH(tanggal_lulus) = 7 THEN "Juli"
        WHEN MONTH(tanggal_lulus) = 8 THEN "Agustus"
        WHEN MONTH(tanggal_lulus) = 9 THEN "September"
        WHEN MONTH(tanggal_lulus) = 10 THEN "Oktober"
        WHEN MONTH(tanggal_lulus) = 11 THEN "November"
        WHEN MONTH(tanggal_lulus) = 12 THEN "Desember"
    END as bulan, COUNT(siswa_id) as jumlah_siswa, tanggal_lulus');
        $this->db->from('tb_kelulusan tk');
        $this->db->join('tb_siswa ts', 'ts.id_siswa = tk.siswa_id', 'left');
        $this->db->where('YEAR (tanggal_lulus) =', date('Y'));
        $this->db->group_by('MONTH (tanggal_lulus)');
        return $this->db->get();
    }

}