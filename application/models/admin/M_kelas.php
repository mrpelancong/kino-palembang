<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kelas extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getKelas()
    {
        $this->db->select('*, count(id_siswa) as jumlah_siswa');
        $this->db->from('tb_kelas');
        $this->db->join('tb_siswa', 'kelas_id=id_kelas', 'left');
        $this->db->group_by('id_kelas');
        return $this->db->get()->result();
    }

}