<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_siswa extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getSiswa()
    {
        $this->db->select('*');
        $this->db->from('tb_siswa');
        $this->db->join('tb_kelas', 'kelas_id=id_kelas', 'left');
        $this->db->join('tb_kelulusan', 'id_siswa=siswa_id', 'left');
        $this->db->group_by('id_siswa');
        return $this->db->get()->result();
    }

}