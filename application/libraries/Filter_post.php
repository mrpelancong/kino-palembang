<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Tekno Pertiwi - Software dan Marketting Devlopments
 * @version    1.0.0
 * @author     Friski Kasviko | https://facebook.com/friski.kasviko | friski26kasviko@gmail.com | 0822 8756 5502
 * @copyright  (c) 2020
 * @link       https://teknopertiwi.com
 *
 */

class Filter_post{
    public function __construct() {
        $this->CI = &get_instance();
        $this->CI_URI = &get_instance();
    }

    function filter($data_post){
        $cek_post = array();
        foreach (array_keys($data_post) as $key) {
            $post = (($this->CI->input->post($key) == null) || ($this->CI->input->post($key) == '')) ? '0' : '1';
            if ($this->CI->input->post($key) != 'action') {
                array_push($cek_post, $post);
            }
            // -------------------------------------
        }
        return (in_array('0', $cek_post)) ? false : $data_post ;
    }
 }