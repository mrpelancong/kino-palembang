<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * INTIME - Integrated Transportation Management
 * Tekno Pertiwi - Software dan Marketting Devlopments
 * @version    1.0.0
 * @author     Friski Kasviko | https://facebook.com/friski.kasviko | friski26kasviko@gmail.com | 0822 8756 5502
 * @copyright  (c) 2020
 * @link       https://teknopertiwi.com
 *
 */

class Datatable {
    public function __construct() {
        $this->CI = &get_instance();
        $this->CI_URI = &get_instance();
    }

}