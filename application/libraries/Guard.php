<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * INTIME - Integrated Transportation Management
 * Tekno Pertiwi - Software dan Marketting Devlopments
 * @version    1.0.0
 * @author     Friski Kasviko | https://facebook.com/friski.kasviko | friski26kasviko@gmail.com | 0822 8756 5502
 * @copyright  (c) 2020
 * @link       https://teknopertiwi.com
 *
 */

class Guard {
    public function __construct() {
        $this->CI = &get_instance();
        $this->CI_URI = &get_instance();
        $this->CI->load->model([
            '_partials/M_menu',
        ]);
    }

    public function Guard_menu()
    {
        $level = $this->CI->session->userdata('id_akses');
        $record_num = $this->CI_URI->uri->total_segments();
        $last_uri = $this->CI_URI->uri->segment($record_num);

        $data = $this->CI->M_menu->cek_menu($level, $this->CI_URI->uri->uri_string());
        // var_dump($data);
        // exit();
        if (empty($data)) {
            if (empty($this->CI->session->userdata('id_akses'))) {
                redirect('auth');
            } else {
                redirect($this->CI->session->userdata('first_page'));
            }
        }
    }
}