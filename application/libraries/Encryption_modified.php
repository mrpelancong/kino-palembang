<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * 
 *
 * @author    Indra Maulana
 * 
 */

// How To Use 

// $acakkarakter 	= $this->encryption_modified->acakkarakter();
// $acakangka 		= $this->encryption_modified->acakangka();
// $encript			= $this->encryption_modified->encrypt2($this->encryption_modified->encrypt($password,$acakkarakter),$acakangka);
// $hasil_token 	= $this->encryption_modified->decrypt($this->encryption_modified->decrypt2($encript,$acakangka),$acakkarakter);

// ===============================================================================================================================

class Encryption_modified {

	private $_CI;

	public function __construct(){
		$this->_CI = &get_instance();
	}

	public function encrypt($val,$ky) 
	{ 
		$key="\1\0\1\0\1\0\0\1\1\0\0\0\1\1\0\1";
		for($a=0;$a<strlen($ky);$a++) 
			$key[$a%16]=chr(ord($key[$a%16]) ^ ord($ky[$a])); 
		$mode=MCRYPT_MODE_ECB; 
		$enc=MCRYPT_RIJNDAEL_128; 
		$val=str_pad($val, (16*(floor(strlen($val) / 16)+(strlen($val) % 16==0?2:1))), chr(16-(strlen($val) % 16))); 
		return mcrypt_encrypt($enc, $key, $val, $mode, mcrypt_create_iv( mcrypt_get_iv_size($enc, $mode), MCRYPT_DEV_URANDOM)); 
	} 

	public function decrypt($val,$ky) 
	{ 
		$key="\1\0\1\0\1\0\0\1\1\0\0\0\1\1\0\1";
		for($a=0;$a<strlen($ky);$a++) 
			$key[$a%16]=chr(ord($key[$a%16]) ^ ord($ky[$a])); 
		$mode = MCRYPT_MODE_ECB; 
		$enc = MCRYPT_RIJNDAEL_128; 
		$dec = @mcrypt_decrypt($enc, $key, $val, $mode, @mcrypt_create_iv( @mcrypt_get_iv_size($enc, $mode), MCRYPT_DEV_URANDOM ) ); 
		return rtrim($dec,(( ord(substr($dec,strlen($dec)-1,1))>=0 and ord(substr($dec, strlen($dec)-1,1))<=16)? chr(ord( substr($dec,strlen($dec)-1,1))):null)); 
	} 

	public function encrypt2($val,$ky)
	{
		$h='';
		$temp='';
		$hasil='';
		if ($val==''){
			$val='<Kosong>';
		}
		for ($i=strlen($val)-1;$i>=0;$i--)
		{
			$h=substr($val,$i,1);
			$temp=ord($h)+$ky;
			$hasil=$hasil.$temp;
		}
		return $hasil;
	}

	public function decrypt2($val,$ky)
	{
		$h='';
		$temp=0;
		$hasil='';
		$pj = strlen($val);
		for ($i=$pj-4;$i>=0;$i=$i-4)
		{
			$h=substr($val,$i,4);		
			$temp=$h-$ky;
			$hasil=$hasil.chr($temp);
		}
		if ($hasil=='<Kosong>' ){
			$hasil='';
		}
		return $hasil;
	}

	public function acakkarakter($length = 12, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ')
	{
		if($length > 0)
		{
			$len_chars = (strlen($chars) - 1);
			$the_chars = $chars{rand(0, $len_chars)};
			for ($i = 1; $i < $length; $i = strlen($the_chars))
			{
				$r = $chars{rand(0, $len_chars)};
				if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
			}

			return $the_chars;
		}
	}

	public function acakangka($length = 4, $chars = '12345678')
	{
		if($length > 0)
		{
			$len_chars = (strlen($chars) - 1);
			$the_chars = $chars{rand(0, $len_chars)};
			for ($i = 1; $i < $length; $i = strlen($the_chars))
			{
				$r = $chars{rand(0, $len_chars)};
				if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
			}
			
			return $the_chars;
		}
	}
}
?>