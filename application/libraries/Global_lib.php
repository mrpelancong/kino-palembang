<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Tekno Pertiwi - Software dan Marketting Devlopments
 * @version    1.0.0
 * @author     Friski Kasviko | https://facebook.com/friski.kasviko | friski26kasviko@gmail.com | 0822 8756 5502
 * @copyright  (c) 2020
 * @link       https://teknopertiwi.com
 *
 */

class Global_lib{
    public function __construct() {
        $this->CI = &get_instance();
        $this->CI_URI = &get_instance();
         $this->CI->load->model([
            'M_master',
        ]);
    }

    function load_data($table, $select="*", $where=[], $join=[], $join_style='inner', $group_by=null, $order=[], $limit=null)
    {
        return $this->CI->M_master->getData($table, $select, $where, $join, $join_style, $group_by, $order, $limit);
    }

 }