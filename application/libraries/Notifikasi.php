<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Tekno Pertiwi - Software dan Marketting Devlopments
 * @version    1.0.0
 * @author     Friski Kasviko | https://facebook.com/friski.kasviko | friski26kasviko@gmail.com | 0822 8756 5502
 * @copyright  (c) 2020
 * @link       https://teknopertiwi.com
 *
 */

class Notifikasi{
    public function __construct() {
        $this->CI = &get_instance();
        $this->CI_URI = &get_instance();
    }

    function hasil($msg, $status_code, $type, $data_result=null)
    {
        $hasil = array( 
            "msg"           => $msg,
            "status_code"   => $status_code,
            "type"          => $type, //success, info, danger, warning 
            "timestamp"     => date('Y-m-d H:i:s'),
            "data"          => $data_result,
        );

        return $this->CI->session->set_flashdata('hasil', $hasil);
    }
 }