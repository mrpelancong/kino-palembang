<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * INTIME - Integrated Transportation Management
 * Tekno Pertiwi - Software dan Marketting Devlopments
 * @version    1.0.0
 * @author     Friski Kasviko | https://facebook.com/friski.kasviko | friski26kasviko@gmail.com | 0822 8756 5502
 * @copyright  (c) 2020
 * @link       https://teknopertiwi.com
 *
 */

class Menu {

    /**
     * Reference to CodeIgniter instance
     *
     * @var object
     */
    protected $CI;

    /**
     * Class Constructor
     *
     * @return Void
     */
    public function __construct() {
        $this->CI = &get_instance();
        $this->CI_URI = &get_instance();
        $this->CI->load->model([
            '_partials/M_menu',
        ]);
    }
    public function page_load($data= [])
    {
        $this->CI->load->view('_partials/header');
        $this->CI->load->view('_partials/breadcrumb');
        $this->CI->load->view('_partials/sidenav', $this->get_menu());
        $this->CI->load->view($this->nama_hal(), $data); //dinamis page
        $this->CI->load->view('_partials/footer');
    }
    public function nama_hal()
    {
        $uri = $this->CI_URI->uri->uri_string();
        if ($uri == "") { 
            $nama_hal = 'login_view';
        } else {
            $nama_hal = $this->cek_file($uri);
        }

        return $nama_hal;
    }


    public function get_menu(){
        $akses_id = $this->CI->session->userdata('id_akses');
        if (!empty($this->CI->M_menu->get_group($akses_id))) {
            $group = $this->CI->M_menu->get_group($akses_id);
            for ($i=0; $i < count($group); $i++) { 
                $data['group'] = $group;
                $data['menu'][$group[$i]->id_group_menu] = $this->CI->M_menu->get_menu($akses_id, "group_menu_id", $group[$i]->id_group_menu);
            }
        } else {
            $data['group'] = [];
            $data['menu'] = [];
        }

        return $data;
    }

    public function get_m_in(){
        $akses_id = $this->CI->session->userdata('id_akses');
        $segmen = $this->uri->segment(3);
        $s_menu = $this->M_master->get_data('tb_menu', 'param_get', $segmen);
        if ($s_menu != []) {
            $id_menu = $s_menu[0]->id_menu;
        } else {
            $id_menu = 0;
        }
        $where_m_user = array(
            'menu_id'       => $id_menu,
            'akses_id'      => $akses_id,
            'type'          => 'SIDE', 
        );
        $s_group = $this->M_master->get_dinamis('tb_menu_user', $where_m_user);
        if ($s_group != []) {
            $id_group_menu = $s_group[0]->group_id;
        } else {
            $id_group_menu = 0;
        }

        return $id_group_menu;
    }

    function cek_file($segmen){
        if (is_file(APPPATH.'views/'.$segmen.'.php') == true) {
            $hal = $segmen;
        } else {
            $hal = 'errors/_404';
        }
        return $hal;
    }

 }