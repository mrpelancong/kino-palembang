<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller { 

    function __construct(){
        parent::__construct();
        $this->load->model(array(
            'admin/M_kelas', 
            'admin/M_siswa', 
        ));
        
        $this->load->library(array('menu', 'guard', 'notifikasi'));
    }

    public function index($debug=null)
    {
        // cek akses halaman
        $this->guard->Guard_menu();
        // view Halaman
        $data = array(
            'page_akses'    =>  $this->uri->uri_string(), 
            'page_data'     =>  $this->M_siswa->getSiswa(), 
            'kelasKino'     =>  $this->M_kelas->getKelas(), 
        );

        $this->menu->page_load($data);
    }

    function tambah_data()
    {
        $data = $_POST;
        // -------------
        $insert_action = $this->M_master->insert_data('tb_siswa', $data);
        if ($insert_action) {
            $this->notifikasi->hasil('Data Berhasil Disimpan', 200,'success', $insert_action);
            echo "<script>window.location=document.referrer;</script>";
        } else {
            $this->notifikasi->hasil('Data Gagal Disimpan', 201,'danger', null);
            echo "<script>window.location=document.referrer;</script>";
        }
    }

    function update_data()
    {
        $data = array(
            'kelas_id' => $this->input->post('kelas_id'), 
            'nik_siswa' => $this->input->post('nik_siswa'), 
            'nama_siswa' => $this->input->post('nama_siswa'), 
            'tanggal_bergabung' => $this->input->post('tanggal_bergabung'), 
        );
        $where = array(
            'id_siswa' => $this->input->post('id_siswa'), 
        );
        // -------------
        $update_action = $this->M_master->update_data('tb_siswa', $data, $where);
        if ($update_action) {
            $this->notifikasi->hasil('Data Berhasil Update', 200,'success', $update_action);
            echo "<script>window.location=document.referrer;</script>";
        } else {
            $this->notifikasi->hasil('Data Gagal Update', 201,'danger', null);
            echo "<script>window.location=document.referrer;</script>";
        }
    }


    function hapus_data($value)
    {
        $hapus_action = $this->M_master->delete_data('tb_siswa', 'id_siswa', $value);
        if ($hapus_action) {
            $this->notifikasi->hasil('Data Berhasil Dihapus', 200,'success', $hapus_action);
            echo "<script>window.location=document.referrer;</script>";
        } else {
            $this->notifikasi->hasil('Data Gagal Dihapus', 201,'danger', null);
            echo "<script>window.location=document.referrer;</script>";
        }
    }

    
}