<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Json_req extends REST_Controller { 
    function __construct(){
        parent::__construct();
        $this->load->model(array(
            'M_master',
        ));
        
        $this->load->library(array('guard'));
    }

    public function index_post()
    {
        // $this->guard->Guard_menu();
        // ------------------------------------------------------------
        $table      = $this->input->post('table');
        $req_data   = $this->input->post('req_data');
        $select = '*';

        if (!empty($this->input->post('params'))) {
            $where  = array(
                $this->input->post('params') => $this->input->post('value'), 
            );
            // ------
            if (!empty($this->input->post('params2'))) {
                $where  += [$this->input->post('params2') => $this->input->post('value2')];
            }
        } else {
            $where = [];
        }
        if (!empty($this->input->post('join'))) {
            $join = array(
                $this->input->post('join') => $this->input->post('join_keys'), 
            );
        } else {
            $join= [];
        }
        
        // ------------------------------------------
        $json_data = $this->load_data($table, $select, $where, $join);
        switch ($req_data) {
            case 'arr':
                $data_tmp = $json_data;
                break;
            case 'obj':
                $data_tmp = (count($json_data) > 0 ? (Object) $json_data[0]: null);
                break;
            
            default:
                $data_tmp = (count($json_data) > 0 ? (Object) $json_data[0]: null);
                break;
        }
        $data = array(
            'json_data' => $data_tmp, 
        );

        if (!empty($data['json_data'])) {
            return $this->hasil('Request Berhasil', true, $data, 200);
        } else {
            return $this->hasil('Request Gagal, Data Kosong', false, $data, 201);
        }
    }

    function load_data($table, $select="*",  $where=[], $join = [], $join_style = 'left', $group_by=null,  $order=[])
    {
        return $this->M_master->getData($table, $select, $where, $join, $join_style, $group_by, $order)->result();
    }

    private function hasil($msg, $data_status, $data_result, $http_result)
    {
        $this->response(
                array( 
                    "msg"         => $msg,
                    "data_status" => $data_status, 
                    "data_jumlah" => count($data_result),
                    "data" => $data_result,
                    "s_kode" => $http_result
                ), $http_result
            );
    }
}