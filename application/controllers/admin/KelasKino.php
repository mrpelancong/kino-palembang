<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelasKino extends CI_Controller { 

    function __construct(){
        parent::__construct();
        $this->load->model(array(
            'admin/M_kelas', 
        ));
        
        $this->load->library(array('menu', 'guard', 'notifikasi'));
    }

    public function index($debug=null)
    {
        // cek akses halaman
        $this->guard->Guard_menu();
        // view Halaman
        $data = array(
            'page_akses' => $this->uri->uri_string(), 
            'page_data' => $this->M_kelas->getKelas(), 
        );

        $this->menu->page_load($data);
    }

    function tambah_data()
    {
        $data = $_POST;
        // -------------
        $insert_action = $this->M_master->insert_data('tb_kelas', $data);
        if ($insert_action) {
            $this->notifikasi->hasil('Data Berhasil Disimpan', 200,'success', $insert_action);
            echo "<script>window.location=document.referrer;</script>";
        } else {
            $this->notifikasi->hasil('Data Gagal Disimpan', 201,'danger', null);
            echo "<script>window.location=document.referrer;</script>";
        }
    }

    function update_data()
    {
        $data = array(
            'nama_kelas' => $this->input->post('nama_kelas'), 
        );
        $where = array(
            'id_kelas' => $this->input->post('id_kelas'), 
        );
        // -------------
        $update_action = $this->M_master->update_data('tb_kelas', $data, $where);
        if ($update_action) {
            $this->notifikasi->hasil('Data Berhasil Update', 200,'success', $update_action);
            echo "<script>window.location=document.referrer;</script>";
        } else {
            $this->notifikasi->hasil('Data Gagal Update', 201,'danger', null);
            echo "<script>window.location=document.referrer;</script>";
        }
    }


    function hapus_data($value)
    {
        $hapus_action = $this->M_master->delete_data('tb_kelas', 'id_kelas', $value);
        if ($hapus_action) {
            $this->notifikasi->hasil('Data Berhasil Dihapus', 200,'success', $hapus_action);
            echo "<script>window.location=document.referrer;</script>";
        } else {
            $this->notifikasi->hasil('Data Gagal Dihapus', 201,'danger', null);
            echo "<script>window.location=document.referrer;</script>";
        }
    }

    
}