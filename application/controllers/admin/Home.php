<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller { 
	function __construct(){
		parent::__construct();
		$this->load->model(array('admin/M_home', 'admin/M_siswa'));
	}

	public function index($debug=null)
	{
		// cek akses halaman
		$this->guard->Guard_menu();
		
		 $data = array(
            'page_akses'    =>  $this->uri->uri_string(), 
            'daftar_siswa'     =>  $this->M_siswa->getSiswa(), 
            'chartBar'     =>  $this->M_home->get_barChartYearly()->result(), 
        );

		 // $this->cek_data->vardump($data['chartBar']);
		$this->menu->page_load($data);
	}

	function lulus_siswa($id_siswa)
    {
        $data = array(
            'siswa_id' => $id_siswa, 
            'tanggal_lulus' => date("Y-m-d"), 
        );
        // -------------
        $this->M_master->insert_data('tb_kelulusan', $data);
        $insert_action = $this->db->insert_id();
        if ($insert_action) {
            $this->notifikasi->hasil('Siswa Berhasil Diluluskan', 200,'success', NULL);
            echo "<script>window.location=document.referrer;</script>";
        } else {
            $this->notifikasi->hasil('Periksa Jaringan Internet Anda', 201,'danger', null);
            echo "<script>window.location=document.referrer;</script>";
        }
    }
}