<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Global_control extends CI_Controller { 

    function __construct(){
        parent::__construct();
        $this->load->model(array(
            'M_master',
        ));
        
        $this->load->library(array('guard','notifikasi'));
    }

    // index = view pertama sekali halaman dibuka
    public function index()
    {
        // cek akses halaman
        $this->guard->Guard_menu();
    }

    function hapus_data($table, $params, $value)
    {
        $hapus_action = $this->M_master->delete_data($table, $params, $value);
        if ($hapus_action) {
            $this->notifikasi->hasil('Data Berhasil Dihapus', 200,'success', $hapus_action);
            echo "<script>window.location=document.referrer;</script>";
        } else {
            $this->notifikasi->hasil('Data Gagal Dihapus', 201,'danger', null);
            echo "<script>window.location=document.referrer;</script>";
        }
    }

    function aktivasi($token)
    {
        $data = array(
            'status' => 'aktif', 
        );
        $where = array(
            'nama_pengguna' => base64_decode($token), 
        );
        $update = $this->M_master->update_data('tb_user', $data, $where);
        if ($update) {
            redirect(site_url('/Auth/registrasiBerhasil'));
        } else {
            echo 'EXPIRED TOKEN - PLEASE CONTACT YOUR OPERATOR :)';
        }
    }
    
}