<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * INTIME - Integrated Transportation Management
 * Tekno Pertiwi - Software dan Marketting Devlopments
 * @version    1.0.0
 * @author     Friski Kasviko | https://facebook.com/friski.kasviko | friski26kasviko@gmail.com | 0822 8756 5502
 * @copyright  (c) 2020
 * @link       https://teknopertiwi.com
 *
 */
class Auth extends CI_Controller { 
	function __construct(){
		parent::__construct();
		$this->load->model(array(
				'_partials/M_menu',
				'M_login'
				));
        $this->load->library(array('menu', 'guard', 'notifikasi'));
	}

	public function index($debug=null, $parent_id=null)
	{
		$data = [];
		$this->load->view('_partials/header');
		$this->load->view('_partials/_breadcrumb', $data); 
		$this->load->view('login_view', $data); 
		$this->load->view('_partials/footer');
	}

	public function registrasiBerhasil($debug=null, $parent_id=null)
	{
		$data = [];
		$this->load->view('_partials/header');
		$this->load->view('_partials/_breadcrumb', $data); 
		$this->load->view('template/registerBerhasil', $data); 
		$this->load->view('_partials/footer');
	}

	public function login()
	{
		$nama_pengguna = $this->security->xss_clean($this->input->post('nama_pengguna'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$user = $this->M_login->getLoginUser($nama_pengguna)->row();
		if (empty($user)) {
			$this->notifikasi->hasil('Username atau Password salah', 304,'danger', null);
			redirect(site_url());
		}
		if ($password == $user->password) {
			$data = [
				"id_akses" 			=> $user->id_akses,
				"id_user" 			=> $user->id_user,
				"nama_pengguna" 	=> $user->nama_pengguna,
				"first_page"		=> $user->halaman_utama,
				"hak_akses" 		=> $user->akses,
			];
		}else{				
			$this->notifikasi->hasil('Username atau Password salah', 304,'danger', null);
			redirect(site_url());	
		}
		$this->session->set_userdata($data);
        $this->notifikasi->hasil('Login Berhasil', 200,'success', null);
		redirect(base_url($data['first_page']), 'refresh');
	}

	public function gantiPassword()
	{
		$password_tmp = $this->input->post('password');
		$password_tmp2 = $this->input->post('password2');
		if ($password_tmp === $password_tmp2) {
			$password = $password_tmp2;
			// $password = password_hash($password_tmp, PASSWORD_DEFAULT);

			$data = array(
				'password' => $password,
			);
			$where = array(
				'id_user' => $this->session->userdata('id_user')
			);

			$updatePassword = $this->M_master->update_data('tb_user', $data, $where);
			if ($updatePassword) {
				$this->notifikasi->hasil('Update Password Berhasil', 200, 'success', null);
            	echo "<script>window.location=document.referrer;</script>";
			} else {
				$this->notifikasi->hasil('Update Password Gagal', 201,'danger', null);
            	echo "<script>window.location=document.referrer;</script>";
			}
		} else {
			$this->notifikasi->hasil('Pasword Tidak Sama Ulangi Kembali', 201,'danger', null);
            echo "<script>window.location=document.referrer;</script>";
		}
	}

	public function logout(){
		session_destroy();
		redirect();
	}


}