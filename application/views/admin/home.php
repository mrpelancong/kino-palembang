<div class="row">
  <div class="col-md-12">
    <div class="tile p-0">
      <div class="folder-head font-weight-bold border-success border-left">
        Daftar Siswa Kino
      </div>
      <div class="tile-body p-2 border-primary border-left">
        <div class="table-responsive">
          <table class="table table-bordered table-sm table-hover tableData">
            <thead>
                <tr>
                    <th width="5%"><center>No.</center></th>
                    <th>NIK</th>
                    <th>Kelas</th>
                    <th>Tanggal Bergabung</th>
                    <th>Tanggal Kelulusan</th>
                    <th><center>Aksi</center></th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                <?php foreach ($daftar_siswa as $key => $value): ?>
                    <?php if(empty($value->tanggal_lulus)): ?>
                    <tr>
                        <td><?php echo $i++ ?></td>
                        <td><strong><?php echo $value->nik_siswa ?></strong> <br> <?php echo $value->nama_siswa ?></td>
                        <td><?php echo $value->nama_kelas ?></td>
                        <td><?php echo $value->tanggal_bergabung ?></td>
                        <td>
                            <?php echo empty($value->tanggal_lulus) ? 'Dalam Proses Pembelajaran' : $value->tanggal_lulus ?>
                        </td>
                        <td class="text-center">
                            <a href="<?php echo base_url('admin/Home/lulus_siswa') ?>/<?php echo $value->id_siswa ?>" class="text-decoration-none btn-edit btn-sm btn-success" >
                                <i class="fa fa-graduation-cap"></i> Lulus
                            </a>
                        </td>
                    </tr>
                    <?php endif; ?>
                <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div>
    <canvas id="myChart" width="1100" height="400"></canvas>
  </div>
</div>


<script type="text/javascript">
  var ctx = document.getElementById('myChart');
  let arrData = [];
  let labelData = [];
  <?php foreach ($chartBar as $key => $value): ?>
    arrData.push(<?php echo $value->jumlah_siswa ?>)
    labelData.push('<?php echo $value->bulan ?>')
  <?php endforeach ?>
  function dynamicColors() {
    var r = Math.floor(Math.random() * 255);
    var g = Math.floor(Math.random() * 255);
    var b = Math.floor(Math.random() * 255);
    return "rgba(" + r + "," + g + "," + b + ", 0.5)";
  }
  function poolColors(a) {
    var pool = [];
    for(i = 0; i < a; i++) {
        pool.push(dynamicColors());
    }
    return pool;
  }
  var myChart = new Chart(ctx, {
      type: 'horizontalBar',
      data: {
          labels: labelData,
          datasets: [{
              label: 'Grafik Kelulusan Tahun <?php echo date('Y') ?>',
              data: arrData,
              backgroundColor: poolColors(arrData.length),
              borderColor: poolColors(arrData.length),
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
  });
</script>