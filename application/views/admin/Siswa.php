
<div class="container-fluid" id="content_tambah_form">
    <h2>Rumah Private - Kino Palembang</h2>
    <p>Tambah, Edit, Hapus dan lihat seluruh data Siswa Kino</p>
   
    <button class="btn btn-primary" data-toggle='modal' data-target='#addData'>
        <i style="vertical-align: initial !important;" class="fa fa-plus"></i>
        Tambah Siswa Kino
    </button>
</div>
<br>
<div class="container-fluid p-6 my-6">
    <div class="row" >
        <div class="table-responsive" style="overflow: hidden;">
            <table class="table table-bordered table-sm table-hover tableData">
                <thead>
                    <tr>
                        <th width="5%"><center>No.</center></th>
                        <th>NIK</th>
                        <th>Kelas</th>
                        <th>Tanggal Bergabung</th>
                        <th>Tanggal Kelulusan</th>
                        <th width="5%"><center>Aksi</center></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($page_data as $key => $value): ?>
                        <tr>
                            <td><?php echo $i++ ?></td>
                            <td><strong><?php echo $value->nik_siswa ?></strong> <br> <?php echo $value->nama_siswa ?></td>
                            <td><?php echo $value->nama_kelas ?></td>
                            <td><?php echo $value->tanggal_bergabung ?></td>
                            <td>
                                <?php echo empty($value->tanggal_lulus) ? 'Dalam Proses Pembelajaran' : $value->tanggal_lulus ?>
                            </td>
                            <td class="text-center">
                                <a href="#" class="text-decoration-none btn-edit" data-tb="tb_siswa" data-params="id_siswa" data-id="<?php echo $value->id_siswa ?>" data-toggle="modal" data-target="#editData">
                                    <i class="fa fa-edit"></i> 
                                </a>
                                <a href="#" class="text-decoration-none text-danger" data-toggle='modal' data-target='#modalHapus' data-action="<?php echo base_url() ?>admin/Siswa/hapus_data/<?php echo $value->id_siswa ?>">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>  
</div>

    <div id="addData" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <strong class="text-uppercase">Data Siswa Kino</strong>
          </div>
          <div class="modal-body">
            <form method="POST" action="<?php echo base_url() ?>admin/Siswa/tambah_data" >
               <div class="form-row">
                <div class="col-md-6 mb-3">
                  <label>Nama Siswa Kino</label>
                  <input type="text" name="nik_siswa" class="form-control" placeholder="NIK Siswa">
                </div>
                <div class="col-md-6 mb-3">
                  <label>Nama Siswa Kino</label>
                  <input type="text" name="nama_siswa" class="form-control" placeholder="Nama Siswa">
                </div>
                <div class="col-md-6 mb-3">
                  <label>Tanggal Bergabung</label>
                  <input type="date" name="tanggal_bergabung" class="form-control">
                </div>
                <div class="col-md-6 mb-3">
                  <label>Kelas Yang Diikuti</label>
                  <select name="kelas_id" class="form-control" required="">
                  	<option selected="" disabled="">Pilih Kelas</option>
                  	 <?php foreach ($kelasKino as $key => $value): ?>
                  	 	<option value="<?php echo $value->id_kelas ?>"><?php echo $value->nama_kelas ?></option>
                  	 <?php endforeach ?>
                  </select>
                </div>
                <div class="col-md-12 text-right">
                <button class="btn btn-primary" type="submit"> <i class="fa fa-paper-plane"></i> Simpan Data</button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
              <strong><i><?php echo SITE_NAME ?></i></strong>
          </div> 
        </div>
      </div>
    </div>

    <div id="editData" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <strong class="text-uppercase">Update Siswa Kino</strong>
          </div>
          <div class="modal-body">
            <form method="POST" action="<?php echo base_url() ?>admin/Siswa/update_data" >
               <div class="form-row">
                <div class="col-md-6 mb-3">
                  <label>Nama Siswa Kino</label>
                  <input type="text" name="id_siswa" hidden="">
                  <input type="text" name="nik_siswa" class="form-control" placeholder="NIK Siswa">
                </div>
                <div class="col-md-6 mb-3">
                  <label>Nama Siswa Kino</label>
                  <input type="text" name="nama_siswa" class="form-control" placeholder="Nama Siswa">
                </div>
                <div class="col-md-6 mb-3">
                  <label>Tanggal Bergabung</label>
                  <input type="date" name="tanggal_bergabung" class="form-control">
                </div>
                <div class="col-md-6 mb-3">
                  <label>Kelas Yang Diikuti</label>
                  <select name="kelas_id" class="form-control" required="">
                  	<option selected="" disabled="">Pilih Kelas</option>
                  	 <?php foreach ($kelasKino as $key => $value): ?>
                  	 	<option value="<?php echo $value->id_kelas ?>"><?php echo $value->nama_kelas ?></option>
                  	 <?php endforeach ?>
                  </select>
                </div>
                <div class="col-md-12 text-right">
                <button class="btn btn-primary" type="submit"> <i class="fa fa-paper-plane"></i> Update Data</button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
              <strong><i><?php echo SITE_NAME ?></i></strong>
          </div> 
        </div>
      </div>
    </div>


<script type="text/javascript">
    $('#editData').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var table = button.data('tb')
        var params = button.data('params')
        var value = button.data('id')
        $.ajax({
              url: base_url+"admin/json_req",
              type: "POST",
              data : { "type": "client", 
                "table": table, 
                "params": params, 
                "value": value, 
                },
              success: function (ajaxData){
                if (ajaxData.data_status) {
                    var keys = Object.keys(ajaxData.data.json_data);
                    var value = Object.values(ajaxData.data.json_data);
                    for (var i = keys.length - 1; i >= 0; i--) {
                        $("input[name='"+keys[i]+"'").val(value[i]);
                        $("select[name='"+keys[i]+"'").val(value[i]);
                    }
                } else {
                    setTimeout(function(){
                        $("#editData").modal('hide');
                        setTimeout(function(){
                            message('Data Tidak Ditemukan!', 'warning', 'false');
                        }, 500)
                    }, 1500)
                }
                
              }
          })
      })
</script>