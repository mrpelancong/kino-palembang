<!-- Cek Siapa yang akses -->
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <style type="text/css">
        .app-content {
          margin-left: 0px !important;
        }

        h3 img.animated {
          animation-duration: 4s;
          animation-iteration-count: infinite;
        }

        .form-signin {
          max-width: 350px;
          margin: 0 auto;
        }
        .form-signin .checkbox {
          font-weight: 400;
        }
        .form-signin .form-control {
          position: relative;
          box-sizing: border-box;
          height: auto;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="email"] {
          margin-bottom: -1px;
          border-bottom-right-radius: 0;
          border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }

    </style>
    <form class="form-signin" method="POST" action="<?php echo base_url('Auth/login') ?>">
        <div class="tile shadow-sm p-0">
            <div class="folder-head text-center">
                <strong>Login</strong>
            </div>
            <div class="tile-body p-2">
                  <h3 class="h3 mb-3 font-weight-normal text-center">
                    <img class="" src="<?php echo base_url('assets/kino-img/logo.jpg') ?>" width="40%">
                  </h3>
                  <div class="form-group">
                    <small id="emailHelp" class="form-text text-muted">Username</small>
                    <input type="text" name="nama_pengguna" class="form-control" id="username" placeholder="Enter Username" autofocus required>
                  </div>
                  <div class="form-group">
                    <small id="passwordHelp" class="form-text text-muted">Password</small>
                    <input type="password" name="password" class="form-control" id="password"  placeholder="Enter Password" required>
                  </div>
                  <div class="form-group">
                    <button id="login" name="login" href="#" class="btn btn-success btn-block" >
                      <i class="fa fa-unlock"></i> Login
                    </button>
                  </div>
            </div>
            <div class="tile-footer text-center p-2">
              <small><?php echo SITE_NAME ?></small>
            </div>
        </div>
    </form>

