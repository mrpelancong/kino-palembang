<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta property="og:type" content="website">
	<meta property="og:title" content="Rumah Private - Kino Palembang">
	<meta property="og:description" content="Rumah Private Kino adalah lembaga pendidikan nonformal yang bergerak dibidang bimbingan belajar dan konsultasi tes, didirikan pada tahun 2005 di Palembang. Rumah Private Kino menyediakan program yang lengkap untuk semua bidang tes,  mulai dari tes Polri, TNI, BUMN, CPNS ataupun Sekolah Ikatan Dinas dengan tujuan untuk melahirkan orang-orang yang berprestasi dan kompeten di bidangnya">
	<title>
		Rumah Private - Kino Palembang
	</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- End of Datepicker -->

  <!-- Main CSS-->
  <link rel="icon" type="icon" href="<?php echo base_url('assets/kino-img/logo.jpg') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/bs4.4.1/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/main.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/sweetalert.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome/css/font-awesome.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animate.css') ?>">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.css">
  <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
  
  <script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js') ?>"></script>
  <!-- datepicker -->
  	<link rel="stylesheet" href="<?php echo base_url('assets/css/datepicker/bootstrap-datepicker.min.css') ?>"> 
  	<script src="<?php echo base_url('assets/js/datepicker/bootstrap-datepicker.min.js') ?>"></script>
  	<!-- end datepicker -->
  	<script>
  		$(function() {
  			$("#datepicker").datepicker({
  				format: 'dd-mm-yyyy',
  				autoclose: true,
  				todayBtn: true,
  			});
  		});
  	</script>

  	<script>
  		$(function() {
  			$(".datepicker").datepicker({
  				format: 'yyyy-mm-dd',
  				autoclose: true,
  				todayBtn: true,
  			});

  			$(".datepicker-lahir").datepicker({
  				format: 'yyyy-mm-dd',
  				autoclose: true,
  				todayBtn: true,
  				startView: 2,
  			});
  		});
  	</script>

  	<script src="<?php echo base_url('assets/bs4.4.1/js/bootstrap.min.js') ?>"></script>
  	<script src="<?php echo base_url('assets/js/popper.min.js') ?>"></script>
  	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  	<script src="<?php echo base_url('assets/notify/bootstrap-notify.js') ?>"></script>
  	<!-- Page specific javascripts-->
  	<script src="<?php echo base_url('assets/js/plugins/jquery.dataTables.min.js') ?>"></script>
  	<script src="<?php echo base_url('assets/js/plugins/dataTables.bootstrap.min.js') ?>"></script>

  	<!-- chart -->

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
  <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
  <!-- select2 -->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
	<script type="text/javascript"> var base_url ='<?php echo base_url() ?>';</script>

</head>

<body class="app sidebar-mini rtl pace-done">

  <!-- checking action -->