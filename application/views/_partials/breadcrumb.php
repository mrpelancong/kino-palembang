
<!-- Navbar-->
<header class="app-header bg-primary shadow-sm">
  <a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
  <span style="margin-top: 15px;" class="font-weight-bold text-white d-none d-sm-block">
    <?php echo SITE_NAME ?> 
  </span>
  <ul class="app-nav">
    <li class="dropdown">
      <a class="app-nav__item text-decoration-none" href="" data-toggle="modal" data-target='#modalPassword'>
        <i class="fa fa-key fa-lg"></i> 
      </a>
    </li>
    <li class="dropdown">
      <a class="app-nav__item text-decoration-none" href="" data-toggle="modal" data-target='#modalLogout' aria-label="Open Profile Menu">
        <i class="fa fa-power-off fa-lg"></i> 
      </a>
    </li>
  </ul>
</header>
  <!-- End Menu Header -->