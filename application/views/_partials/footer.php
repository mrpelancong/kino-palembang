</div>
<!--  Modal -->
<?php $this->load->view('_partials/modal.php') ?>

<!-- end modal -->

<script src="<?php echo base_url('assets/js/main.js') ?>"></script>
<script src="<?php echo base_url('assets/js/hapus.js') ?>"></script>
<script src="<?php echo base_url('assets/js/admin.js') ?>"></script>

<!-- The javascript plugin to display page loading on top-->
<script src="<?php echo base_url('assets/js/plugins/pace.min.js') ?>"></script>
<script>
	$(document).ready(function() {
		var table = $('#example').DataTable({
			responsive: true
		});
		$(".pengembangan").click(function() {
			message('XCOVID19','Sistem Dalam pengembangan', 'warning', 'false');
		})
	});
</script>
<script type="text/javascript">
	$('.tableData').DataTable({
		processing: true,
		pageLength: 5
	});
	$('.tableDataNosearch').DataTable({
		paging :   false,
		processing: true,
		searching: false,
		order : [[0, "desc"]]
	});
</script>
<?php $msg = $this->session->flashdata('hasil'); ?>

<!-- hasil eksekusi Query -->
<?php if ($this->session->flashdata('hasil')) : ?>
	<script type="text/javascript">
		message('<strong>KINO PALEMBANG - </strong>', '<?php echo $msg['msg'] ?>', '<?php echo $msg['type'] ?>', true);
	</script>
<?php endif ?>


</body>

</html>