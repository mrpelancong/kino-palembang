  <div class="modal fade" id="modalLogout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Yakin akan keluar?</h5> <br>
          
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <strong>Pastikan data anda terlah tersimpan semuanya!</strong><br>
          <small></small>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" href="<?php echo base_url('Auth/logout') ?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- hapus -->
  <div id="modalHapus" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Konfirmasi</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <strong> Apakah Anda yakin ingin menghapus data ini ? <br> </strong>
          <div class="mgs_penting"></div>
        </div>
        <div class="modal-footer">
          <a href="javascript:;" class="btn btn-danger" id="hapus-true-data"> <i class="fa fa-info-circle"></i> Hapus</a>
          <button type="button" class="btn btn-primary" data-dismiss="modal"> <i class="fa fa-check-circle"></i> Tidak</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pastikan Password anda tetap terjaga!</h5> <br>
        </div>
        <form method="post"  action="<?= base_url() ?>/auth/gantiPassword">
            <div class="modal-body">
              <div class="row">
                  <div class="col-md-12 mb-2">
                      <label for="usr"><b>Masukkan Password Baru<span class="text-danger">*)</span></b></label>
                      <input type="password" name="password" class="form-control" required autocomplete="off" placeholder="Password">
                  </div>
                  <div class="col-md-12 mb-2">
                      <label for="usr"><b>Ulangi Password<span class="text-danger">*)</span></b></label>
                      <input type="password" name="password2" class="form-control" required autocomplete="off" placeholder="Password">
                  </div>
              </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success" type="submit"> <i class="fa fa-paper-plane"></i> Ganti Password</button>
            </div>
        </form>
      </div>
    </div>
  </div>