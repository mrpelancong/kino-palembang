    <?php 
      $last_uri = $this->uri->uri_string();
     ?>
    <aside class="app-sidebar bg-dark">
      <center><img class="" src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png" width="80%"></center>
      <span class="badge badge-info ml-1 mr-1 d-block app-menu__label"> <i class="fa fa-dot-circle-o text-warning"></i> <?php echo $this->input->ip_address(); ?></span>
      <hr>
      <ul class="app-menu">
        <?php foreach ($group as $group_data) : ?>
          <!-- Group Menu -->
          <?php if ($group_data->nama_group == 'umum') : ?>
            <?php foreach ($menu[$group_data->id_group_menu] as $menu_data) : ?>
              <?php if ($menu_data->type == 'SIDE') : ?>
                <li class="treeview">
                  <a class="app-menu__item <?php if ($menu_data->param_get == $last_uri) : ?>
                    active
                    <?php endif ?>" href="<?php echo base_url($menu_data->param_get) ?>">
                    <i class="app-menu__icon <?php echo $menu_data->icon ?>"></i>
                    <span class="app-menu__label">
                      <?php echo $menu_data->menu ?>
                    </span>
                  </a>
                </li>
              <?php endif ?>
            <?php endforeach ?>
          <?php else : ?>
            <li class="treeview">
              <a class="app-menu__item" href="#" data-toggle="treeview">
                <i class="app-menu__icon <?php echo $group_data->icon_group ?>"></i>
                <span class="app-menu__label"><?php echo $group_data->nama_group; ?></span>
                <i class="treeview-indicator fa fa-angle-right"></i>
              </a>
              <ul class="treeview-menu">
                <!-- Menu -->
                <?php foreach ($menu[$group_data->id_group_menu] as $menu_data) : ?>
                  <li>
                    <a class="treeview-item <?php if ($menu_data->param_get == $last_uri) : ?>
                      active
                      <?php endif ?>" href="<?php echo base_url($menu_data->param_get) ?>">

                      <span class="app-menu__label">
                        <i class="app-menu__icon <?php echo $menu_data->icon ?>"></i>
                        <?php
                        echo $menu_data->menu;
                        ?>
                      </span>
                    </a>
                  </li>
                  <?php if ($menu_data->param_get == $last_uri) : ?>
                    <script type="text/javascript">
                      $(".active").parent("li").parent("ul").parent("li").addClass("is-expanded");
                    </script>
                  <?php endif ?>
                <?php endforeach ?>
              </ul>
            </li>
          <?php endif ?>
        <?php endforeach ?>
        <style type="text/css">
          .copyright_item.copyright, .copyright_item:hover, .copyright_item:focus{
            background: #0d121400 !important;
            border-left-color: #ffc107 !important;
            color: #28a745 !important;
          }
        </style>
        <li class="treeview fixed-bottom" style="width: 230px !important;">
          <a class="app-menu__item copyright_item" href="#">
            <span class="app-menu__label copyright">
              <small><strong>2021 &copy; Rumah Private Kino</strong></small>
            </span>
          </a>
        </li>
      </ul>
    </aside>
  <!-- End Menu -->
  <div class="app-content bg-white">
    <div class="app-title bg-info text-white p-2">
      <span class="font-weight-bold d-none d-md-block"><i class="fa fa-folder"></i> <?php echo ucfirst($this->uri->uri_string()); ?></span>
      <span class="pull-right mr-4"> <i class="fa fa-hand-peace-o"></i> Selamat datang! <strong><?php echo $this->session->userdata('hak_akses') ?></strong></span>
    </div>