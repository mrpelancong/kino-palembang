-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: 103.77.106.66    Database: teknope1_kino
-- ------------------------------------------------------
-- Server version	5.5.5-10.0.38-MariaDB-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_akses`
--

DROP TABLE IF EXISTS `tb_akses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_akses` (
  `id_akses` int(11) NOT NULL AUTO_INCREMENT,
  `akses` varchar(100) NOT NULL,
  `keterangan_akses` text NOT NULL,
  PRIMARY KEY (`id_akses`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_akses`
--

LOCK TABLES `tb_akses` WRITE;
/*!40000 ALTER TABLE `tb_akses` DISABLE KEYS */;
INSERT INTO `tb_akses` VALUES (1,'Superadmin','Level Tertinggi'),(2,'enum','Enumerator / Kader');
/*!40000 ALTER TABLE `tb_akses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_group_menu`
--

DROP TABLE IF EXISTS `tb_group_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_group_menu` (
  `id_group_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_group` varchar(100) NOT NULL,
  `icon_group` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id_group_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_group_menu`
--

LOCK TABLES `tb_group_menu` WRITE;
/*!40000 ALTER TABLE `tb_group_menu` DISABLE KEYS */;
INSERT INTO `tb_group_menu` VALUES (1,'umum','fa fa-check',0),(2,'Master Data','fa fa-database',0);
/*!40000 ALTER TABLE `tb_group_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_kelas`
--

DROP TABLE IF EXISTS `tb_kelas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_kelas` (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelas` varchar(100) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_kelas`
--

LOCK TABLES `tb_kelas` WRITE;
/*!40000 ALTER TABLE `tb_kelas` DISABLE KEYS */;
INSERT INTO `tb_kelas` VALUES (1,'Bimbel Akademik','2021-08-08 02:54:05','2021-08-08 03:03:48'),(2,'Bimbel SKD','2021-08-08 03:04:27','2021-08-08 03:04:27'),(3,'Bimbel TPA','2021-08-08 03:04:39','2021-08-08 03:04:39'),(4,'Bimbel Psikotest','2021-08-08 03:04:52','2021-08-08 03:04:52');
/*!40000 ALTER TABLE `tb_kelas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_kelulusan`
--

DROP TABLE IF EXISTS `tb_kelulusan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_kelulusan` (
  `id_kelulusan` int(11) NOT NULL AUTO_INCREMENT,
  `siswa_id` int(11) NOT NULL,
  `tanggal_lulus` date NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kelulusan`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_kelulusan`
--

LOCK TABLES `tb_kelulusan` WRITE;
/*!40000 ALTER TABLE `tb_kelulusan` DISABLE KEYS */;
INSERT INTO `tb_kelulusan` VALUES (1,1,'2021-08-08','2021-08-08 03:46:48','2021-08-08 03:46:48'),(2,2,'2021-08-08','2021-08-08 04:06:09','2021-08-08 04:06:09'),(3,3,'2021-08-08','2021-08-08 04:06:10','2021-08-08 04:06:10'),(4,4,'2021-09-08','2021-08-08 04:06:12','2021-08-08 04:07:22'),(5,5,'2021-09-08','2021-08-08 04:06:14','2021-08-08 04:07:22'),(6,6,'2021-09-08','2021-08-08 04:06:15','2021-08-08 04:07:22'),(7,7,'2021-10-08','2021-08-08 04:06:31','2021-08-08 04:07:22'),(8,8,'2021-10-08','2021-08-08 04:06:33','2021-08-08 04:07:22');
/*!40000 ALTER TABLE `tb_kelulusan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_menu`
--

DROP TABLE IF EXISTS `tb_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(50) NOT NULL,
  `param_get` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `urutan` int(11) NOT NULL,
  `status` enum('Active','Shutdown') NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_menu`
--

LOCK TABLES `tb_menu` WRITE;
/*!40000 ALTER TABLE `tb_menu` DISABLE KEYS */;
INSERT INTO `tb_menu` VALUES (1,'Home','admin/home','fa fa-home',1,'Active'),(2,'Master Wilayah','admin/wilayah','fa fa-map-marker',1,'Active'),(3,'Kelas Kino','admin/kelasKino','fa fa-users',1,'Active'),(4,'Siswa Kino','admin/Siswa','fa fa-graduation-cap',2,'Active');
/*!40000 ALTER TABLE `tb_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_menu_user`
--

DROP TABLE IF EXISTS `tb_menu_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_menu_user` (
  `id_active` int(11) NOT NULL AUTO_INCREMENT,
  `akses_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `group_menu_id` int(11) NOT NULL,
  `type` enum('SIDE','IN') DEFAULT 'SIDE',
  PRIMARY KEY (`id_active`),
  KEY `menu_id` (`menu_id`),
  KEY `group_id` (`group_menu_id`),
  KEY `level` (`akses_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_menu_user`
--

LOCK TABLES `tb_menu_user` WRITE;
/*!40000 ALTER TABLE `tb_menu_user` DISABLE KEYS */;
INSERT INTO `tb_menu_user` VALUES (1,1,1,1,'SIDE'),(2,1,3,2,'SIDE'),(3,1,4,2,'SIDE'),(4,1,5,1,'SIDE');
/*!40000 ALTER TABLE `tb_menu_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_siswa`
--

DROP TABLE IF EXISTS `tb_siswa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_siswa` (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_id` int(11) NOT NULL,
  `nik_siswa` varchar(16) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_bergabung` date NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_siswa`
--

LOCK TABLES `tb_siswa` WRITE;
/*!40000 ALTER TABLE `tb_siswa` DISABLE KEYS */;
INSERT INTO `tb_siswa` VALUES (1,4,'1603032605970002','Friski Kasviko','2021-08-08 03:23:20','2021-08-08 03:23:20','2021-08-08'),(2,4,'1603032605970001','asfw','2021-08-08 04:05:39','2021-08-08 04:05:39','2021-08-08'),(3,1,'1603032605970003','SADFAS','2021-08-08 04:05:39','2021-08-08 04:05:39','2021-08-08'),(4,2,'1603032605970006','ASDFA','2021-08-08 04:05:39','2021-08-08 04:05:39','2021-08-08'),(5,3,'1603032605970009','BGXCBX','2021-08-08 04:05:39','2021-08-08 04:05:39','2021-08-08'),(6,2,'1603032605970008','KLHKJ','2021-08-08 04:05:39','2021-08-08 04:05:39','2021-08-08'),(7,3,'1603032605970007','KLSDA','2021-08-08 04:05:39','2021-08-08 04:05:39','2021-08-08'),(8,1,'1603032605970004','ADQWQ','2021-08-08 04:05:39','2021-08-08 04:05:39','2021-08-08');
/*!40000 ALTER TABLE `tb_siswa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `akses_id` int(11) NOT NULL,
  `halaman_utama` varchar(100) DEFAULT 'admin/page',
  `email_reset` varchar(100) NOT NULL,
  `nama_pengguna` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL DEFAULT '',
  `status` enum('aktif','nonaktif') NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`),
  KEY `akses_id` (`akses_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` VALUES (1,1,'admin/home','teknopertiwi@gmail.com','superadmin','qwerty','aktif','2021-06-20 14:40:09','2021-06-20 14:40:09');
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'teknope1_kino'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-08  4:57:12
